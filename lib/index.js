"use strict;"
const {sources, workspace} = require('coc.nvim')
const {spawn} = require('child_process')
const readline = require('readline')

exports.activate = async context => {
  let beforePattern = new RegExp( '^.*([:,]\\s+|\\s@)' )
  const config = workspace.getConfiguration('mail')
  const lbdbLocalMethods = config.get('lbdbLocalMethods')
  const lbdbRemoteMethods = config.get('lbdbRemoteMethods')
  const aliases = config.get('aliases')

  let mailSources = [{
      name: 'L',
      priority: 99,
      lbdbMethods: lbdbLocalMethods
    }]
  if( Object.keys(aliases) )
    mailSources = mailSources.concat({
      name: 'A',
      priority: 99,
      doComplete: async function() {
	return {
	  items: Object.keys(aliases).map( k => {
	    let m = (typeof( aliases[k] ) == "string") ? aliases[k] : aliases[k].join(", ")
	    return {
	      word: m,
	      info: m,
	      abbr: truncate(m),
	      filterText: `${k} ${m}`,
	      menu: this.menu
	    }
	  })
	}
      }
    })
  if( lbdbRemoteMethods !== '' )
    mailSources = mailSources.concat({
      name: 'R',
      priority: 10,
      lbdbMethods: lbdbRemoteMethods
    })

  mailSources.forEach( s => context.subscriptions.push(
    sources.createSource({
      // unique id
      name: s.name+'-email',
      // used in menu
      shortcut: s.name+'@',
      filetypes: ['mail'],
      // not trigger when trigger patterns doesn't match
      triggerOnly: false,
      priority: s.priority,
      triggerPatterns: [
	/^(Bcc|Cc|From|Reply-To|To):/,
	/^(Bcc|Cc|From|Reply-To|To):.*,/,
	/(^|\s)@/
      ],
      triggerCharacters: '@',
      doComplete: s.doComplete ? s.doComplete : async function(opt) {
	let input = opt.line.substring( 0, opt.colnr)
	  .replace( beforePattern, '' ).trim()
	if( input === '' )
	  return { items: [] }

	let cmd = ['lbdbq', input]
	if( s.lbdbMethods !== '' )
	  cmd = ['env', `LBDB_OVERRIDE_METHODS=${s.lbdbMethods}`].concat(cmd)
	let matches = await getAddresses(cmd)

	// workspace.showMessage( `${s.name}: input='${input}'` )
	return {
	  items: matches.map(m => {
	    return {
	      word: `${quoted_name(m[1])} <${m[0]}>`,
	      abbr: `${truncate(m[0])} ${m[1]}`,
	      info: truncate(m[2]),
	      filterText: `${m[0]} ${m[1]} ${m[2]}`,
	      menu: this.menu
	    }
	  })
	}
      },
      onCompleteDone: async function(item, opt) {
	// workspace.showMessage( `onCompleteDone ${s.name}` )
	let {nvim} = workspace
	let { linenr, col, input, line } = opt
	let pre = line.substring( 0, col).match( beforePattern )[0]
	if( pre.endsWith('@') ) pre=pre.slice(0, -1)
	let after = line.substring( col + input.length )
	await nvim.call( 'coc#util#setline',
	  [linenr, `${pre}${item.word}${after},`])
	await nvim.call( 'cursor',
	  [linenr, Buffer.byteLength( `${pre}${item.word}`) + 2] )
      }
    })
  ))
}

async function getAddresses(cmd) {
  let result = []
  return new Promise((resolve, reject) => {
    const p = spawn(cmd[0], cmd.slice(1) )
    const rl = readline.createInterface(p.stdout)
    p.on('error', reject)
    rl.on('line', line => {
      if (line.startsWith('lbdbq:')) return
      let [email, name, comment] = line.split('\t')
      result.push([email, name, comment])
    })
    rl.on('close', () => {
      resolve(result)
    })
  })
}

function quoted_name(s) {
  return s.match( new RegExp('^[^"].*,') ) ? `"${s}"` : s 
}

function truncate(s) {
  let replacements = {
    '/@(...)...*' : '@$1…',
    'Department': 'Dept'
  }
  let ret = s
  for (let k in replacements) {
    ret = ret.replace(
      k.startsWith('/') ? new RegExp(k.substring(1) ) : k,
      replacements[k] )
  }
  return ret
}
