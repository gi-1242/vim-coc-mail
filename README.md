# coc-mail: Email address completion source for [coc.nvim]

* Type anywhere in `To:`, `Cc:` etc. mail headers to auto-complete email
  addresses (using [lbdb]). Alternately trigger completion with `@`.

* Configure using `mail.lbdbLocalMethods` (local fast), and
  `mail.lbdbRemoteMethods` (remote, slower)

* Add custom aliases using `mail.aliases`. (This is useful for groups, as
  [lbdb] doesn't handle them well.)

          "mail.aliases": {
            "alias1" : "address1@domain.org",
            "alias2" : [ "list@of.emails", ... ]
          }

## Installation

* First install [coc.nvim] for fast, instant incremental completion.

* Install and configure [lbdb] to your taste.

* Clone this repository into your Vim/NeoVim package directory. (On my
  system I put it in `~/.config/nvim/pack/local/start/coc-mail`).
  Alternately you can use a plugin manager such as [packer].

* If you use [mutt], then you may want to bypass the compose menu and go
  directly to composing your email. Put the following in your
  `~/.mutt/muttrc`:

        set autoedit = yes
        set edit_headers = yes

### Configuring lbdb

[lbdb] can get email addresses from various sources including your [mutt]
aliases, email and LDAP.

* Email addresses from your outgoing mails will be completed, provided you add
  the `m_inmail` to `METHODS` in `~/.lbdbrc`.

* To manually add email addresses from your inbox in [mutt] to the completion
  list (using [lbdb]), use the following macros in your mutt config files:

        macro index \ea ":set pipe_decode pipe_decode_weed=no pipe_split\
            \n;|lbdb-fetchaddr\n:set pipe_decode=no pipe_decode_weed pipe_split=no\
            \n:echo 'Fed messages to lbdb'\n" \
            "Scrape addresses from tagged messages using lbdb"
        macro index A ":set pipe_decode pipe_decode_weed=no\
            \n|lbdb-fetchaddr\n:set pipe_decode=no pipe_decode_weed\
            \n:echo 'Fed message to lbdb'\n" \
            "Scrape addresses from message using lbdb"
        macro pager A ":set pipe_decode pipe_decode_weed=no\
            \n|lbdb-fetchaddr\n:set pipe_decode=no pipe_decode_weed\
            \n:echo 'Fed message to lbdb'\n" \
            "Scrape addresses from message using lbdb"

    Pressing A in the index or pager will add all addresses from the current
  message using [lbdb].

* You could get more ambitious and write a macro so that mutt feeds any email
  you read to [lbdb].

* You can scrape email addresses from all emails in your IMAP cache using:

        for f in ~/.cache/mutt/messages/imaps:gi..@gmail.com/INBOX; do
            lbdb-fetchaddr < $f
        done

* Getting email addresses from all your *incoming* mail is not worth it IMO.
  First the database gets full of spam/useless addresses very quickly. Second,
  it requires some work to setup. If you want to set it up, here are a few
  options you have:

  * Write a mutt macro that feeds every email you read to [lbdb]
  * Write a `.procmail` rule and merge `~/.lbdb/m_inmail.utf-8` from the IMAP
    server to with to the one on your local machine.
  * Running `offlineimap` or some other IMAP synchronization tool to get a
      local copy of your mail, and then feeding that to [lbdb].

* Edit `~/.mutt_ldap_query.rc` with your organizations LDAP settings (copy
  the template from `/etc/lbdb_ldap.rc`, and change the server/search info).

## TODO

* Figure out how to complete phrases better, so you can search more naturally.

* Completion at the start of lines that start with whitespaces aren't cleaned
  up properly.

## LICENCE

Distributed under the MIT Licence.
Copyright (c) 2022 Gautam Iyer

[coc.nvim]: https://github.com/neoclide/coc.nvim
[lbdb]: https://github.com/RolandRosenfeld/lbdb
[packer]: https://github.com/wbthomason/packer.nvim
[mutt]: http://www.mutt.org/
